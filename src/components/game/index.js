import React from 'react';
import Slots from '../slots';
import Result from '../result';
import Debug from '../debug';

function Game() {
  return (
    <div className="game">
      <Slots />
      <Result />
      <Debug />
    </div>
  );
}

export default Game;