import React, { useEffect } from 'react';
import { useSelector } from "react-redux";
import { DELAY } from '../../const';
import Coins from '../coins';

function Result() {
  const { slots, amount } = useSelector(state => state.play);

  useEffect(() => {
    setTimeout(() => {
    }, DELAY);
  }, [slots]);

  return (
    <div className="result">
      <Coins />
      <h2>{amount || 0}</h2>
    </div>
  );
}

export default Result;