import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { debugModeStop } from '../../actions';

function updateReelPositon() {
  return (Math.floor(Math.random() * 10) + 10) * 120;
}

function positonInDebugMode(pos, bar, row) {
  const current = pos / 120 % 5;
  const r = row > -1 ? row : 0;
  let result;

  if (current === bar) {
    result = 5 - r;
  }

  if (current > bar) {
    result = ((5 - current) % 5) + bar - r;
  }

  if (current < bar) {
    result = bar - current - r;
  }

  return pos + (120 * (result < 0 ? 4 : result));
}

function Reel(props) {
  const [pos, setPos] = useState(0);
  const [index, setIndex] = useState(0);
  const dispatch = useDispatch();
  const { debugActive, bar, row } = useSelector(state => state.debug);
  const { stop, onStop, speed } = props;

  useEffect(() => {
    let offset;
    if (!stop) {
      offset = updateReelPositon();
      setIndex(index + offset);
    }

    if (debugActive) {
      dispatch(debugModeStop());
      offset = positonInDebugMode(pos, bar, row);
      setIndex(offset);
    }

    if (pos < index) {
      setTimeout(() => {
        setPos(pos + 10);
      }, speed)
    } else {
      onStop(index, offset);
    }
  }, [stop, onStop, index, setIndex, speed, debugActive, dispatch, pos, bar, row]);

  return (
    <div className="reel" style={{ backgroundPosition: `0 -${pos}px` }} />
  )
}

export default Reel;