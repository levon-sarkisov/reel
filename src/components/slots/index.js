import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { startRunReels } from '../../actions';
import { DELAY } from '../../const';

import Reel from '../reel';

function getCurrent(index, offset) {
  return (index + offset) / 120 % 5;
}

function Slots() {
  const [stop, setStop] = useState(true);
  const [disabled, setDisabled] = useState(false);
  const [selectedSlots, setSelectedSlots] = useState([]);
  const { amount } = useSelector(state => state.coins);
  const dispatch = useDispatch();

  useEffect(() => {
    console.log('selected slots', selectedSlots);
  }, [selectedSlots]);

  const onPlay = () => {
    setStop(false);
    setDisabled(true);
    setTimeout(() => setDisabled(false), DELAY);
  }
  
  const onStop = (index, offset) => {
    if (offset) {
      setSelectedSlots(state => {
        if (state.length === 3) {
          state = [];
          state.push(getCurrent(index, offset));
        } else {
          state.push(getCurrent(index, offset));
          if (state.length === 3) {
            dispatch(startRunReels(state));
          }
        }
        return state;
      });
      
      setStop(true);
    }
  }

  return (
    <div className="slot">
      <div className="slot_items">
        <Reel speed={1} stop={stop} onStop={onStop} />
        <Reel speed={2} stop={stop} onStop={onStop} />
        <Reel speed={3} stop={stop} onStop={onStop} />
      </div>
      <p className={`button${amount === 0 ? ' disabled' : ''}`}>
        <button onClick={onPlay}>PLAY</button>
      </p>
    </div>
  );
}

export default Slots;