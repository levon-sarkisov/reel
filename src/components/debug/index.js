import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { debugModeStart, debugModeStop } from '../../actions';
import { DELAY } from '../../const';

function Debug() {
  const [active, setActive] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [bar, setBar] = useState(0);
  const [row, setRow] = useState(0);
  const dispatch = useDispatch();
  const { slots } = useSelector(state => state.play);
  const { amount } = useSelector(state => state.coins);

  useEffect(() => {
    if (!active) {
      dispatch(debugModeStop());
    }
  }, [active, dispatch]);

  useEffect(() => {
    if (slots) {
      setDisabled(true);
      setTimeout(() => {
        setDisabled(false);
      }, DELAY);
    }
  }, [slots, amount]);

  const onDebugModeStart = () => {
    setActive(!active);
  }

  const runDebug = () => {
    dispatch(debugModeStart({ bar, row }));
  }

  const onSelectBar = e => {
    const { value } = e.target;
    setBar(parseInt(value, 10));
  }

  const onSelectRow = e => {
    const { value } = e.target;
    setRow(parseInt(value, 10));
  }

  return (
    <div className={`debug${amount === 0 ? ' disabled' : ''}`}>
      <p><button className={`${active && 'debug__active'}`} onClick={onDebugModeStart}>Start debug mode</button></p>
      {active &&
        <>
          <select onChange={onSelectBar}>
            {
              ['bar', '2xbar', '3xbar', '7', 'cherry'].map((bar, i) =>
                <option value={i} key={i}>{bar}</option>
              )
            }
          </select>
          <select onChange={onSelectRow} >
            {['top', 'center', 'bottom'].map((row, i) =>
              <option value={i} key={i}>{row}</option>
            )}
          </select>
          <p className={`button ${disabled ? 'disabled' : ''}`}>
            <button onClick={runDebug}>RUN</button>
          </p>
        </>
      }
    </div >
  );
}

export default Debug;