import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { noCoins } from '../../actions';

function Coins() {
  const [coins, setCoins] = useState(10);
  const dispatch = useDispatch();
  const { slots } = useSelector(state => state.play);

  useEffect(() => {
    if (slots) {
      setCoins(c => c - 1);
    }
  }, [slots]);

  useEffect(() => {
    if (coins === 0) {
      dispatch(noCoins());
    }
  }, [dispatch, coins]);

  return (
    <div className="coins">
      {coins === 0 
        ? (<h2 className="no-money">no money</h2>)
        : (<h2>{coins}</h2>)
      }
    </div>
  );
}

export default Coins;