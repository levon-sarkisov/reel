/* General
-------------------------------------------------- */
export const DELAY = 1500;

/* Redux constants
-------------------------------------------------- */
export const START_RUN_REELS = 'START_RUN_REELS';
export const NO_COINS = 'NO_COINS';
export const DEBUG_MODE_START = 'DEBUG_MODE_START';
export const DEBUG_MODE_STOP = 'DEBUG_MODE_STOP';