import { createStore, applyMiddleware } from 'redux';
import reducer from '../reducers';

export default function configureStore(data) {
  const middleware = [ 
  ];  

  const store = createStore(
    reducer,
    data,
    applyMiddleware(...middleware),
  );  

  return store;
}
