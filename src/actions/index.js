import {
    START_RUN_REELS,
    NO_COINS,
    DEBUG_MODE_START,
    DEBUG_MODE_STOP
} from '../const';

export const startRunReels = selectedSlots => {
    return {
        type: START_RUN_REELS,
        payload: selectedSlots
    }
}

export const noCoins = () => {
    return {
        type: NO_COINS,
        payload: 0
    }
}

export const debugModeStart = params => {
    return {
        type: DEBUG_MODE_START,
        payload: params
    }
}

export const debugModeStop = () => {
    return {
        type: DEBUG_MODE_STOP,
        payload: null
    }
}