import { combineReducers } from 'redux';
import play from './play';
import coins from './coins';
import debug from './debug';

const reducers = combineReducers({
  play,
  coins,
  debug
});

export default reducers;