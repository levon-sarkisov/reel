import {
    DEBUG_MODE_START,
    DEBUG_MODE_STOP
} from '../../const';

const play = (state = {}, action) => {
    switch (action.type) {
        case DEBUG_MODE_START:
            const { bar, row } = action.payload;
            return Object.assign(
                {}, state, {
                debugActive: true,
                bar,
                row
            },
            );
        case DEBUG_MODE_STOP:
            return Object.assign(
                {}, state, {
                debugActive: false,
                bar: null,
                row: 0,
            },
            );
        default:
            return Object.assign(
                {}, state,
            );
    }
};

export default play;