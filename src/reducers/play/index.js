import {
    START_RUN_REELS,
} from '../../const';

function getWiningPosition(v, index) {
    if (v[0] === index && v[1] === index && v[2] === index) {
        return 0;
    }

    if (v[0] + 1 === index && v[1] + 1 === index && v[2] + 1 === index) {
        return 1;
    }

    if (v[0] + 2 === index && v[1] + 2 === index && v[2] + 2 === index) {
        return 2;
    }

    return null;
}

function getBarsPosition(v, index1, index2) {
    if (v.indexOf(index1) > -1 && v.indexOf(index2) > -1) {
        return true;
    }
    return false;
}

function calcWinningCombinations(v) {
    let amount = 0;

    /* Cherry
    -------------------------------------------------- */
    switch (getWiningPosition(v, 4)) {
        case 0:
            amount += 2000;
            break;
        case 1:
            amount += 1000;
            break;
        case 2:
            amount += 4000;
            break;
        default:
            amount += 0;
    }

    /* Cherry and 7
    -------------------------------------------------- */
    if (getBarsPosition(v, 3, 4)) {
        amount += 75;
    }

    if (getBarsPosition(v, 2, 3)) {
        amount += 75;
    }

    if (getBarsPosition(v, 1, 2)) {
        amount += 75;
    }

    /* 3xbars
    -------------------------------------------------- */

    if (getWiningPosition(v, 2) !== null) {
        amount += 50;
    }

    /* 2xbars
    -------------------------------------------------- */
    if (getWiningPosition(v, 1) !== null) {
        amount += 20;
    }

    /* 1xbars
    -------------------------------------------------- */
    if (getWiningPosition(v, 0) !== null) {
        amount += 10;
    }

    return amount;
}

const play = (state = {}, action) => {
    switch (action.type) {
        case START_RUN_REELS:
            const { payload } = action;
            return Object.assign(
                {}, state, {
                slots: payload,
                amount:  calcWinningCombinations(payload) + state.amount || 0,
            },
            );
        default:
            return Object.assign(
                {}, state,
            );
    }
};

export default play;