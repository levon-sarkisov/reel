import {
    NO_COINS,
} from '../../const';

const coins = (state = {}, action) => {
    switch (action.type) {
        case NO_COINS:
            return Object.assign(
                {}, state, {
                  amount: action.payload,
                },
              );
        default:
            return Object.assign(
                {}, state,
            );
    }
};

export default coins;